Project: Review medical journals Application 
Candidate: Yuri Stefano Scripnic
Date: January , 26th 2017 

1.	Instructions to install and configure any prerequisites or dependencies in order to setup and 
run the given code, that you had to do

The code could be open, build and executed with any environment or IDE with Java,maven and MySQL installed.
The only exception is the lack of instruction regards the need to create a <User Home>/upload directory and 
copy the content of DOCs directory there.
Only reading the code someone could know about this requirement.

2.	Instructions to configure and prepare the source code to build and run properly, that you had to do
The code run without errors, except by the user password for the database.

I had install MySQL before hand, but the root password was not the same. Then I needed to change it in the
 application.properties file.

3.	Assumptions you have made - it is good to explain your thought process and the assumptions you have made

My main suggest about the functionality is about the lack of the Edition entity. The current application 
models the Journal like a final edition, and therefore is not possible to do a search of the editions from 
the same journal. I suggest that is needed to chance the model, and include it. That, of course, will bring 
several changes in the code.
Another issue is a certain indefinition in the category of a document. In medical industry is common to a 
journal have multiple categories, and the specification is not clear about it. 
I think the relationship between the Journal and Category entities needs to change to One to Many.
 That will bring several changes in services and presentation layers to accommodate this.

In the design, except for a small system, I believe it's not a good option to save and read the files 
from the disk.
It will compromise Maintainability because that a machine, or disk, become a single point of failure.
It compromises scalability because you can only grow the storage space vertically, adding more disk.
It's hard to maintain because tge web application and storage shares the same system.
The best option is to use a document-oriented database, like Solr or ElasticSearch, what provide 
a scalability and fault tolerance and can provide future full-text search capabilities.
If full-text searching is not needed, database storage is preferred.

In code , I noticed the lack of modularity and tests in AngularJS application. AngularJS provides a rich 
framework of tools to be used to test, both for Unit and Integration Testing.The lack of tests can compromise any 
Continuous Integration initiative.
Modularity is a main characteristic of AngularJS, and I suggest to use it , to separating the controllers, 
services and utility functions in different files.

It's recommended by Spring Team to use the constructor Dependence Injection, what can improves the code quality.

Optinal class is not correctly used in the code, it needs to check if a value is present (using isPresent()) 
before call the method get().


4.	Any other issues you have faced while completing the assignment
No issues, everything went fine. 

5.	Any constructive feedback for improving the assignment
Even if I am not accepted for the job, I think the task and the time allocated to it were well specified for the position.
Congratulations!