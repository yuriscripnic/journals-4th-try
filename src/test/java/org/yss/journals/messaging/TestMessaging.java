package org.yss.journals.messaging;


import org.yss.journals.Application;
import org.yss.journals.messaging.email.EmailService;
import org.yss.journals.model.Journal;
import org.yss.journals.model.Publisher;
import org.yss.journals.model.User;
import org.yss.journals.repository.PublisherRepository;
import org.yss.journals.service.JournalService;
import org.yss.journals.service.ServiceException;
import org.yss.journals.service.UserService;
import de.saly.javamail.mock2.MailboxFolder;
import de.saly.javamail.mock2.MockMailbox;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.mail.*;
import java.util.Optional;
import java.util.Properties;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class TestMessaging {

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Autowired
    private JournalService journalService;

    @Autowired
    private PublisherRepository publisherRepository;

    @Autowired
    private MessageServiceImpl dailyMessage;


    private final String TEST_EMAIL = "rshiraynne@gmail.com";
    private final String NEW_JOURNAL_NAME = "New Journal";

    private MockMailbox mb;
    private MailboxFolder mf;

    @Before
    public void setup() {
        try {
            mb = MockMailbox.get(TEST_EMAIL);
            mf = mb.getInbox();
            deleteMessages(mf.getMessages());
            mb.getInbox().expunge();
        } catch (Exception ex) {
            fail("Error configuring mock email");
            ex.printStackTrace();
        }

    }

    @Test
    public void sendUserEmail() {
        try {
            User user = getUser("user1");
            emailService.sendEmail(user.getEmail(), "test", "test");

            Folder inbox = getInbox();

            assertEquals(1, inbox.getMessageCount());
            assertNotNull(inbox.getMessage(1));
            deleteMessages(inbox.getMessages());
            inbox.close(true);
        } catch (Exception ex) {
            fail("Email Error:" + ex.getMessage());
            ex.printStackTrace();
        }
    }

    @Test
    public void sendDailyEmail(){

        User user = getUser("publisher2");
        Optional<Publisher> p = publisherRepository.findByUser(user);

        Journal journal = new Journal();
        journal.setName(NEW_JOURNAL_NAME);
        journal.setUuid("SOME_EXTERNAL_ID");
        try {
            journalService.publish(p.get(), journal, 3L);
            //dailyMessage.getDailyMessages().stream()
             //       .forEach(msg -> emailService.sendEmail(msg.getEmail(), msg.getTitle(), msg.getBody()));
            journalService.unPublish(p.get(), journal.getId());

            Folder inbox = getInbox();
            assertEquals(3, inbox.getMessageCount());
            assertNotNull(inbox.getMessage(1));
            deleteMessages(inbox.getMessages());
            inbox.close(true);
        } catch (ServiceException e) {
            fail("Service Error:" + e.getMessage());
        } catch (MessagingException me) {
            fail("Email Error:" + me.getMessage());
        }
    }

    private Folder getInbox() throws MessagingException {
        Session session = Session.getInstance(new Properties());
        final Store store = session.getStore("pop3s");
        store.connect(TEST_EMAIL, null);
        Folder inbox = store.getFolder("INBOX");
        inbox.open(Folder.READ_WRITE);
        return inbox;
    }

    private void deleteMessages(Message[] messages) throws MessagingException {
        for (Message m : messages) {
            m.setFlag(Flags.Flag.DELETED, true);
        }
    }

    protected User getUser(String name) {
        Optional<User> user = userService.getUserByLoginName(name);
        if (!user.isPresent()) {
            fail("user doesn't exist");
        }
        return user.get();
    }

}
