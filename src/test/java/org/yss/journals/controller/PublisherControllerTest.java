package org.yss.journals.controller;

import org.yss.journals.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration

public class PublisherControllerTest {
    public static final String JOURNAL_NAME = "Medicine";
    public static final long CATEGORY_ID = 1L;
    RedirectAttributes attrib;
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private PublisherController publisherController;
    private Principal principal;
    private MockMultipartFile file, fileEmpty;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        principal = SecurityContextHolder.getContext().getAuthentication();
        file = new MockMultipartFile("data", "filename.txt", "text/plain", "data".getBytes());
        fileEmpty = new MockMultipartFile("data", "filename.txt", "text/plain", "".getBytes());
        attrib = new RedirectAttributesModelMap();
    }

    @Test
    @WithUserDetails("publisher1")
    public void testHandleFileUploadSuccess() {
        assertEquals("redirect:/publisher/browse", publisherController.handleFileUpload(JOURNAL_NAME, CATEGORY_ID, file, attrib, principal));
    }

    /*@Test(expected = AccessDeniedException.class)
    //@WithUserDetails("user1")
    @WithMockUser(roles = {"USER"})
    public void testHandleFileUploadUnAuthorizedUserFail() {
        assertEquals ("redirect:/publisher/browse",publisherController.handleFileUpload(JOURNAL_NAME, CATEGORY_ID,file,attrib,principal));
    }*/

    @Test
    @WithUserDetails("publisher1")
    public void testHandleFileUploadFileEmptyFail() {
        assertEquals("redirect:/publisher/publish", publisherController.handleFileUpload(JOURNAL_NAME, CATEGORY_ID, fileEmpty, attrib, principal));
        assertTrue(attrib.getFlashAttributes().get("message").toString().indexOf("because the file was empty") > 0);
    }
}
