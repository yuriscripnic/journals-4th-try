package org.yss.journals.controller;

import org.yss.journals.Application;
import org.apache.commons.io.FileUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.File;
import java.io.IOException;
import java.security.Principal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration

public class JournalControllerTest {
    public static final long VALID_JOURNAL_ID = 1L;
    public static final long INVALID_JOURNAL_ID = 100L;
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private JournalController journalController;

    private Principal principal;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
        principal = SecurityContextHolder.getContext().getAuthentication();
        File testFilesOrigin= new ClassPathResource("upload").getFile();
        File testFilesDestination=new File(Application.ROOT);
        FileUtils.copyDirectory(testFilesOrigin,testFilesDestination);
    }

    @Test
    @WithUserDetails("publisher1")
    public void testRenderDocumentSuccess() throws IOException {
        ResponseEntity response = journalController.renderDocument(principal, VALID_JOURNAL_ID);
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertNotNull(response.getBody());
    }

    @Test
    @WithUserDetails("publisher1")
    public void testRenderDocumentNotFoundFail() throws IOException {
        ResponseEntity response = journalController.renderDocument(principal, INVALID_JOURNAL_ID);
        assertEquals(HttpStatus.NOT_FOUND, response.getStatusCode());
    }
}
