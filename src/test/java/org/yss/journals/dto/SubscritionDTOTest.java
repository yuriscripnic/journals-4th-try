package org.yss.journals.dto;

import org.yss.journals.model.Category;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

public class SubscritionDTOTest {

    public static final String CATEGORY1 = "category1";
    public static final long CATEGORY_ID1 = 1L;

    public static final String CATEGORY2 = "category2";
    public static final long CATEGORY_ID2 = 2L;

    Category category1,category2;

    @Before
    public void setup(){
        category1 = new Category();
        category1.setName(CATEGORY1);
        category1.setId(CATEGORY_ID1);

        category2 = new Category();
        category2.setName(CATEGORY2);
        category2.setId(CATEGORY_ID2);

    }

    @Test
    public void testCreateSubscriptionDTO(){
        SubscriptionDTO dto = new SubscriptionDTO(category1);
        assertEquals(CATEGORY1,dto.getName());
        assertEquals(CATEGORY_ID1,dto.getId());
    }

    @Test
    public void testTwoSubscriptionDTOShouldNotBeEqual(){

        SubscriptionDTO dto1 = new SubscriptionDTO(category1);
        SubscriptionDTO dto2 = new SubscriptionDTO(category2);

        assertFalse(dto1.equals(dto2));
        assertFalse(dto1.hashCode()== dto2.hashCode());
    }
}
