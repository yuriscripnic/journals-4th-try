package org.yss.journals.rest;

import org.yss.journals.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.nio.charset.Charset;
import java.security.Principal;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertFalse;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class JournalRestServiceTest {
    private final MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(), Charset.forName("utf8"));
    private MockMvc mockMvc;
    @Autowired
    private WebApplicationContext webApplicationContext;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    @WithUserDetails("user1")
    public void testBrowse() throws Exception {
        Principal user_principal = SecurityContextHolder.getContext().getAuthentication();
        String result = mockMvc.perform(get("/rest/journals").principal(user_principal))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.*", hasSize(1)))
                .andReturn().getResponse().getContentAsString();
        assertFalse(result.isEmpty());
    }

    @Test
    @WithUserDetails("publisher1")
    public void testPublishList() throws Exception {
        Principal user_principal = SecurityContextHolder.getContext().getAuthentication();
        String result = mockMvc.perform(get("/rest/journals/published").principal(user_principal))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.*", hasSize(3)))
                .andReturn().getResponse().getContentAsString();
        assertFalse(result.isEmpty());
    }

    @Test
    @WithUserDetails("publisher2")
    public void testUnPublish() throws Exception {
        Principal user_principal = SecurityContextHolder.getContext().getAuthentication();
        mockMvc.perform(delete("/rest/journals/unPublish/3").principal(user_principal))
                .andExpect(status().isOk());
    }

    @Test
    @WithUserDetails("user1")
    public void testGetUserSubscriptions() throws Exception {
        Principal user_principal = SecurityContextHolder.getContext().getAuthentication();
        String result = mockMvc.perform(get("/rest/journals/subscriptions").principal(user_principal))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andExpect(jsonPath("$.*", hasSize(5)))
                .andReturn().getResponse().getContentAsString();
        assertFalse(result.isEmpty());
    }

    @Test
    @WithUserDetails("user2")
    public void testSubscribe() throws Exception {
        Principal user_principal = SecurityContextHolder.getContext().getAuthentication();
        mockMvc.perform(post("/rest/journals/subscribe/2").principal(user_principal))
                .andExpect(status().isOk());
    }
}
