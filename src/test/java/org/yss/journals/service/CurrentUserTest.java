package org.yss.journals.service;

import org.yss.journals.model.Role;
import org.yss.journals.model.User;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class CurrentUserTest {


    public static final String USER1 = "user1";
    public static final long USER1_ID = 1L;
    public static final String USER2 = "user2";
    public static final long USER2_ID = 2L;

    private User user1,user2;

    @Before
    public void setup(){
        user1 = new User();
        user1.setId(USER1_ID);
        user1.setLoginName(USER1);
        user1.setPwd("secret");
        user1.setRole(Role.USER);

        user2 = new User();
        user2.setId(USER2_ID);
        user2.setLoginName(USER2);
        user2.setPwd("secret");
        user2.setRole(Role.USER);

    }

    @Test
    public void testCreateCurrentUser(){
        CurrentUser currentUser= new CurrentUser(user1);
        assertEquals(USER1,currentUser.getUsername());
    }

    @Test
    public void testTwoCurrentUserShouldNotBeEqual(){

        CurrentUser currentUser1= new CurrentUser(user1);
        CurrentUser currentUser2= new CurrentUser(user2);

        assertFalse(currentUser1.equals(currentUser2));
        assertFalse(currentUser1.hashCode()== currentUser2.hashCode());
    }
}
