package org.yss.journals.service;

import org.yss.journals.Application;
import org.yss.journals.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
public class UserServiceTest {

    public static final String PUBLISHER2 = "publisher2";
    public static final long INVALID_CATEGORY_ID = 100L;
    public static final long PUBLISHER2_ID = 2L;
    public static final long PUBLISHER2_CATEGORY_ID = 1L;
    public static final String INVALID_PUBLISHER = "publisher100";
    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private UserService userService;

    @Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void testGetUserByLoginNameSuccess() {
        Optional<User> user = userService.getUserByLoginName(PUBLISHER2);
        assertTrue(user.isPresent());
        assertEquals(PUBLISHER2, user.get().getLoginName());
    }

    @Test
    public void testGetUserByLoginNameFail() {
        Optional<User> user = userService.getUserByLoginName(INVALID_PUBLISHER);
        assertFalse(user.isPresent());
    }

    @Test
    public void testSubscribeSuccess() throws ServiceException {
        User user = userService.getUserByLoginName(PUBLISHER2).get();
        userService.subscribe(user, PUBLISHER2_CATEGORY_ID);
    }

    @Test(expected = ServiceException.class)
    public void testSubscribeCategoryNotFoundFail() throws ServiceException {
        User user = userService.getUserByLoginName(PUBLISHER2).get();
        userService.subscribe(user, INVALID_CATEGORY_ID);
    }

    @Test
    public void testFindByIdSuccess() {
        User user = userService.findById(PUBLISHER2_ID);
        assertNotNull(user);
        assertEquals(PUBLISHER2, user.getLoginName());
    }

    @Test
    public void testFindByIdFail() {
        User user = userService.findById(INVALID_CATEGORY_ID);
        assertNull(user);
    }

}
