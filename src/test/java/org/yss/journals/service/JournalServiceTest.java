package org.yss.journals.service;

import org.yss.journals.Application;
import org.yss.journals.model.Journal;
import org.yss.journals.model.Publisher;
import org.yss.journals.model.User;
import org.yss.journals.repository.PublisherRepository;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JournalServiceTest {

    private final static String NEW_JOURNAL_NAME = "New Journal";
    public static final String PUBLISHER1 = "publisher1";
    public static final String PUBLISHER2 = "publisher2";
    public static final String NEW_JOURNAL = "New Journal";
    public static final String MEDICINE_JOURNAL = "Medicine";
    public static final String TEST_JOURNAL = "Test Journal";
    public static final String HEALTH_JOURNAL = "Health";
    public static final String USER1 = "user1";
    public static final String SOME_EXTERNAL_ID = "SOME_EXTERNAL_ID";
    public static final String USER2 = "user2";
    public static final long ENDOCRINOLOGY_CATEGORY_ID = 3L;

    @Autowired
    private JournalService journalService;

    @Autowired
    private UserService userService;

    @Autowired
    private PublisherRepository publisherRepository;

    @Test
    public void browseSubscribedUser() {
        List<Journal> journals = journalService.listAll(getUser(USER1));
        assertNotNull(journals);
        assertEquals(1, journals.size());

        assertEquals(Long.valueOf(1), journals.get(0).getId());
        assertEquals(MEDICINE_JOURNAL, journals.get(0).getName());
        assertEquals(Long.valueOf(1), journals.get(0).getPublisher().getId());
        assertNotNull(journals.get(0).getPublishDate());
    }

    @Test
    public void browseUnSubscribedUser() {
        List<Journal> journals = journalService.listAll(getUser(USER2));
        assertEquals(0, journals.size());
    }

    @Test
    public void listPublisher() {
        User user = getUser(PUBLISHER1);
        Optional<Publisher> p = publisherRepository.findByUser(user);
        List<Journal> journals = journalService.publisherList(p.get());
        assertEquals(3, journals.size());

        assertEquals(Long.valueOf(1L), journals.get(0).getId());
        assertEquals(Long.valueOf(2L), journals.get(1).getId());

        assertEquals(MEDICINE_JOURNAL, journals.get(0).getName());
        assertEquals(TEST_JOURNAL, journals.get(1).getName());
        journals.stream().forEach(j -> assertNotNull(j.getPublishDate()));
        journals.stream().forEach(j -> assertEquals(Long.valueOf(1), j.getPublisher().getId()));

    }

    @Test(expected = ServiceException.class)
    public void publishFail() throws ServiceException {
        User user = getUser(PUBLISHER2);
        Optional<Publisher> p = publisherRepository.findByUser(user);

        Journal journal = new Journal();
        journal.setName(NEW_JOURNAL);

        journalService.publish(p.get(), journal, 1L);
    }

    @Test(expected = ServiceException.class)
    public void publishFail2() throws ServiceException {
        User user = getUser(PUBLISHER2);
        Optional<Publisher> p = publisherRepository.findByUser(user);

        Journal journal = new Journal();
        journal.setName(NEW_JOURNAL);

        journalService.publish(p.get(), journal, 150L);
    }

    @Test
    public void publishSuccess() {
        User user = getUser(PUBLISHER2);
        Optional<Publisher> p = publisherRepository.findByUser(user);

        Journal journal = new Journal();
        journal.setName(NEW_JOURNAL_NAME);
        journal.setUuid(SOME_EXTERNAL_ID);
        try {
            journalService.publish(p.get(), journal, ENDOCRINOLOGY_CATEGORY_ID);
        } catch (ServiceException e) {
            fail(e.getMessage());
        }

        List<Journal> journals = journalService.listAll(getUser(USER1));
        assertEquals(2, journals.size());

        //journals = journalService.publisherList(p.get());
        //assertEquals(2, journals.size());
        //assertEquals(Long.valueOf(3), journals.get(0).getId());
        //assertEquals(Long.valueOf(4), journals.get(1).getId());
        //assertEquals(HEALTH_JOURNAL, journals.get(0).getName());
        //assertEquals(NEW_JOURNAL_NAME, journals.get(1).getName());
        //Because the tests are not atomic, ids and number of items can be different
        journals.stream().forEach(j -> assertNotNull(j.getName()));
        journals.stream().forEach(j -> assertNotNull(j.getPublishDate()));
        //journals.stream().forEach(j -> assertEquals(Long.valueOf(2), j.getPublisher().getId()));
    }

    @Test(expected = ServiceException.class)
    public void unPublishFail() throws ServiceException {
        User user = getUser(PUBLISHER1);
        Optional<Publisher> p = publisherRepository.findByUser(user);
        journalService.unPublish(p.get(), 3L);
    }

    @Test(expected = ServiceException.class)
    public void unPublishFail2() throws ServiceException {
        User user = getUser(PUBLISHER1);
        Optional<Publisher> p = publisherRepository.findByUser(user);
        journalService.unPublish(p.get(), 100L);
    }

    @Test
    public void unPublishSuccess() throws ServiceException {
        User user = getUser(PUBLISHER1);
        Optional<Publisher> p = publisherRepository.findByUser(user);
        journalService.unPublish(p.get(), 1L);

        List<Journal> journals = journalService.publisherList(p.get());
        int numberJournals = journals.size();
        journals = journalService.listAll(getUser(USER1));
        // User1 has at least one journal from publisher1
        assertTrue(numberJournals >= journals.size());
    }

    protected User getUser(String name) {
        Optional<User> user = userService.getUserByLoginName(name);
        if (!user.isPresent()) {
            fail("user doesn't exist");
        }
        return user.get();
    }

}
