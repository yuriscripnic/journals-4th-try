package com.crossover.journals;

import org.yss.journals.Application;
import org.yss.journals.config.MvcConfig;
import org.yss.journals.config.WebSecurityConfig;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT, classes = Application.class)
@ContextConfiguration(classes = {MvcConfig.class, WebSecurityConfig.class})
public class UsersSteps {

    @LocalServerPort
    private int port;

    private WebDriver driver;

    @Given("^A User with username:\"([^\"]*)\" and password:\"([^\"]*)\" login to the site$")
    public void a_User_with_username_and_password_login_to_the_site(String username, String password) throws Throwable {
        driver.get("http://localhost:" + port + "/login");
        driver.findElement(By.xpath("//input[@name='username']")).sendKeys(username);
        driver.findElement(By.xpath("//input[@name='password']")).sendKeys(password);
        driver.findElement(By.xpath("//input[@type='submit']")).submit();
    }

    @Then("^Login success$")
    public void login_success() throws Throwable {
        String title = driver.getTitle();
        System.out.println("Title:" + title);
        assertEquals("Welcome to medical journal system", title);
    }

    @And("^user navigate to View Journals$")
    public void user_navigate_to_view_journals() throws Throwable {

    }

    @Before
    public void setup() {
        //driver = new HtmlUnitDriver();
        System.setProperty("webdriver.chrome.driver", "/usr/local/bin/chromedriver");
        driver = new ChromeDriver();
        System.out.println("port:" + port);
    }

    @After
    public void close() {
        driver.quit();
    }

}
