package org.yss.journals.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

import org.yss.journals.model.Journal;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.yss.journals.model.Category;
import org.yss.journals.model.Subscription;
import org.yss.journals.model.User;
import org.yss.journals.repository.JournalRepository;
import org.yss.journals.repository.UserRepository;
import org.yss.journals.service.CurrentUser;

@Controller
public final class JournalController {

	@Autowired
	private JournalRepository journalRepository;

	@Autowired
	private UserRepository userRepository;

	@ResponseBody
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET, produces = "application/pdf")
	public ResponseEntity renderDocument(@AuthenticationPrincipal Principal principal, @PathVariable("id") Long id)
			throws IOException {
		Optional<Journal> journalOpt = Optional.ofNullable(journalRepository.findOne(id));
		if(!journalOpt.isPresent()) return ResponseEntity.notFound().build();
		Journal journal = journalOpt.get();
		Category category = journal.getCategory();
		CurrentUser activeUser = (CurrentUser) ((Authentication) principal).getPrincipal();
		User user = userRepository.findOne(activeUser.getUser().getId());
		List<Subscription> subscriptions = user.getSubscriptions();
		Optional<Subscription> subscription = subscriptions.stream()
				.filter(s -> s.getCategory().getId().equals(category.getId())).findFirst();
		if ((subscription.isPresent()) || (journal.getPublisher().getId().equals(user.getId()))) {
			File file = new File(PublisherController.getFileName(journal.getPublisher().getId(), journal.getUuid()));
			byte[] byteArray;
			try(InputStream in = new FileInputStream(file)) {
				byteArray = IOUtils.toByteArray(in);
			}
			return ResponseEntity.ok(byteArray);
		} else {
			return ResponseEntity.notFound().build();
		}

	}
}
