package org.yss.journals.service;

import java.util.Optional;

import org.yss.journals.model.User;

public interface UserService {

    Optional<User> getUserByLoginName(String loginName);

    void subscribe(User user, Long categoryId) throws ServiceException;

    User findById(Long id);

}