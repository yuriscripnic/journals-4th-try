package org.yss.journals.service;

import org.yss.journals.model.Role;
import org.yss.journals.model.User;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Objects;

public final class CurrentUser extends org.springframework.security.core.userdetails.User {

	private final User user;

	public CurrentUser(User user) {
		super(user.getLoginName(), user.getPwd(), AuthorityUtils.createAuthorityList(user.getRole().toString()));
		this.user = user;
	}

	public User getUser() {
		return user;
	}

	public Long getId() {
		return user.getId();
	}

	public Role getRole() {
		return user.getRole();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof CurrentUser)) {
			return false;
		}
		if (!super.equals(o)) {
			return false;
		}
		CurrentUser that = (CurrentUser) o;
		return Objects.equals(getUser(), that.getUser());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getUser());
	}
}