package org.yss.journals.service;

import org.yss.journals.model.Journal;
import org.yss.journals.model.Publisher;
import org.yss.journals.model.User;

import java.util.List;

public interface JournalService {

	List<Journal> listAll(User user);

	List<Journal> publisherList(Publisher publisher);

	Journal publish(Publisher publisher, Journal journal, Long categoryId) throws ServiceException;

	void unPublish(Publisher publisher, Long journalId) throws ServiceException;
}
