package org.yss.journals.model;

public enum Role {
	USER, PUBLISHER
}