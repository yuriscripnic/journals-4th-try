package org.yss.journals.repository;

import org.yss.journals.model.Category;
import org.yss.journals.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import org.yss.journals.model.User;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByLoginName(String loginName);

    List<User> findByRole(Role role);

    @Query("select u from User u INNER JOIN u.subscriptions s where s.category = ?1 and u.role='USER'")
    List<User> findBySubscriptionInCategory(Category category);
}
