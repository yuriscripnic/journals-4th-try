package org.yss.journals.repository;

import org.yss.journals.model.Journal;
import org.yss.journals.model.Publisher;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface JournalRepository extends CrudRepository<Journal, Long> {

    Collection<Journal> findByPublisher(Publisher publisher);

    List<Journal> findByCategoryIdIn(List<Long> ids);

    @Query("select j from Journal j where j.publishDate between ?1 and ?2")
    List<Journal> findByDatesBetween(Date departure, Date arrival);

}
