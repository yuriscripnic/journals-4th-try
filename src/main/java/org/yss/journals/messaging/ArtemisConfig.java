package org.yss.journals.messaging;

import org.yss.journals.messaging.email.EmailConfig;
import org.apache.activemq.artemis.api.core.SimpleString;
import org.apache.activemq.artemis.core.settings.impl.AddressSettings;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisConfigurationCustomizer;
import org.springframework.boot.autoconfigure.jms.artemis.ArtemisProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.util.ErrorHandler;

import javax.jms.ConnectionFactory;
import javax.jms.Session;

@Configuration
public class ArtemisConfig {

    private static final Logger log = Logger.getLogger(ArtemisConfig.class);

    public static final String DESTINATION_NAME = "jms.queue.email";
    public static final String DEAD_LETTER_NAME = "jms.queue.deadLetterQueue";




    @Autowired
    private ArtemisProperties artemisProperties;

    @Autowired
    private EmailConfig emailConfig;

    @Bean
    public ArtemisConfigurationCustomizer customizer() {
        return configuration -> {
            try {
                configuration.addAcceptorConfiguration("netty", "tcp://localhost:" + artemisProperties.getPort());

                AddressSettings settings = new AddressSettings();
                SimpleString deadLetterRequest = new SimpleString(DEAD_LETTER_NAME);
                settings.setMaxDeliveryAttempts(emailConfig.getMaxDeliveryAttempts());
                settings.setRedeliveryDelay(emailConfig.getRedeliveryDelay());
                settings.setDeadLetterAddress(deadLetterRequest);
                settings.setExpiryAddress(deadLetterRequest);

                configuration.addAddressesSetting(DESTINATION_NAME, settings);

            } catch (Exception e) {
                throw new RuntimeException("Failed to configure artemis instance", e);
            }
        };
    }

    @Bean
    public JmsListenerContainerFactory<?> jmsFactory(ConnectionFactory connectionFactory,
                                                     DefaultJmsListenerContainerFactoryConfigurer configurer) {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setSessionAcknowledgeMode(Session.AUTO_ACKNOWLEDGE);
        factory.setErrorHandler(errorHandler());
        configurer.configure(factory, connectionFactory);
        return factory;
    }

    @Bean
    public MessageConverter jacksonJmsMessageConverter() {
        MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
        converter.setTargetType(MessageType.TEXT);
        converter.setTypeIdPropertyName("_ype");
        return converter;
    }


    @Bean
    public ErrorHandler errorHandler(){
        return throwable -> log.error(throwable.getMessage());
    }
}
