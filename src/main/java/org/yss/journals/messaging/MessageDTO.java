package org.yss.journals.messaging;

import java.io.Serializable;

public class MessageDTO implements Serializable {

    private static final long serialVersionUID = -7818240090446389611L;

    private String email;
    private String title;
    private String body;

    public MessageDTO() {
    }

    public MessageDTO(String email, String title, String body) {
        this.email = email;
        this.title = title;
        this.body = body;
    }

    public String getEmail() {
        return email;
    }

    public String getTitle() {
        return title;
    }

    public String getBody() {
        return body;
    }
}
