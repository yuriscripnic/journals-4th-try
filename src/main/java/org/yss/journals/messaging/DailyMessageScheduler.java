package org.yss.journals.messaging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class DailyMessageScheduler {

    @Autowired
    private MessageService messageService;

    @Scheduled(cron = "${dailyreport.frequency}")
    public void execute() {
        messageService.sendDailyMessages();
    }
}
