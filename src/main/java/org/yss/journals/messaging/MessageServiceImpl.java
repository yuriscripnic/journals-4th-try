package org.yss.journals.messaging;


import org.yss.journals.messaging.templating.HTMLGenerator;
import org.yss.journals.model.Journal;
import org.yss.journals.model.Role;
import org.yss.journals.model.User;
import org.yss.journals.repository.JournalRepository;
import org.yss.journals.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service
public final class MessageServiceImpl implements MessageService {

    private static final String NEW_JOURNALS_DAILY_REPORT_TITLE = "New Journals , Daily report";
    private static final String NEW_JOURNAL_ADDED_IN_CATEGORY_TITLE = "New Journal added in category: ";
    private static final String MAIL_NEW_JOURNAL_ADDED_MAIL_TEMPLATE = "mail/NewJournalAddedMailTemplate";
    private static final String MAIL_DAILY_MAIL_TEMPLATE = "mail/DailyMailTemplate";
    private static final String DATE_FORMAT = "MM/dd/yyyy";
    private static final String JOURNAL_STATEMENT_FORMAT = "%s in category %s";
    private static final String VARIABLE_NAME = "name";
    private static final String VARIABLE_CATEGORY_NAME = "categoryName";
    private static final String VARIABLE_JOURNAL_NAME = "journalName";
    private static final String VARIABLE_PUBLISH_DATE = "publishDate";
    private static final String VARIABLE_JOURNALS_LIST = "journalsList";
    private JournalRepository journalRepository;
    private UserRepository userRepository;
    private HTMLGenerator htmlTemplate;

    private JmsTemplate jmsTemplate;

    @Autowired
    MessageServiceImpl(JournalRepository journalRepository, UserRepository userRepository, HTMLGenerator htmlTemplate, JmsTemplate jmsTemplate) {
        this.journalRepository = journalRepository;
        this.userRepository = userRepository;
        this.htmlTemplate = htmlTemplate;
        this.jmsTemplate = jmsTemplate;
    }

    public List<MessageDTO> getDailyMessages() {
        List<MessageDTO> messages = new ArrayList<>();
        Date lastDay = getLastDay();
        List<Journal> journals = journalRepository.findByDatesBetween(lastDay, Calendar.getInstance().getTime());
        if (journals != null) {
            List<String> journalsList = new ArrayList<>();
            journals.forEach(x -> journalsList.add(getJournalMessageStatement(x)));
            List<User> users = userRepository.findByRole(Role.USER);
            users.forEach(u -> {
                String body = getDailyMailTemplateBody(lastDay, journalsList, u);
                messages.add(new MessageDTO(u.getEmail(), NEW_JOURNALS_DAILY_REPORT_TITLE, body));
            });
        }
        return messages;
    }

    public List<MessageDTO> getNewJournalMessages(Journal journal) {
        List<MessageDTO> messages = new ArrayList<>();
        String categoryName = journal.getCategory().getName();
        String journalName = journal.getName();
        List<User> users = userRepository.findBySubscriptionInCategory(journal.getCategory());
        users.stream().forEach(u -> {
            String body = getNewJournalAddedMailTemplateBody(categoryName, journalName, u);
            messages.add(new MessageDTO(u.getEmail(), NEW_JOURNAL_ADDED_IN_CATEGORY_TITLE + categoryName, body));
        });
        return messages;
    }

    private String getJournalMessageStatement(Journal x) {
        return String.format(JOURNAL_STATEMENT_FORMAT, x.getName(), x.getCategory().getName());
    }

    private Date getLastDay() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR, -24);
        return calendar.getTime();
    }

    private String getNewJournalAddedMailTemplateBody(String categoryName, String journalName, User u) {
        Context ctx = new Context();
        ctx.setVariable(VARIABLE_NAME, u.getLoginName());
        ctx.setVariable(VARIABLE_CATEGORY_NAME, categoryName);
        ctx.setVariable(VARIABLE_JOURNAL_NAME, journalName);

        return htmlTemplate.generate(MAIL_NEW_JOURNAL_ADDED_MAIL_TEMPLATE, ctx);
    }

    private String getDailyMailTemplateBody(Date lastDay, List<String> journalsList, User u) {
        SimpleDateFormat formatDate = new SimpleDateFormat(DATE_FORMAT);
        Context ctx = new Context();
        ctx.setVariable(VARIABLE_NAME, u.getLoginName());
        ctx.setVariable(VARIABLE_PUBLISH_DATE, formatDate.format(lastDay));
        ctx.setVariable(VARIABLE_JOURNALS_LIST, journalsList);
        return htmlTemplate.generate(MAIL_DAILY_MAIL_TEMPLATE, ctx);
    }

    public void sendNewJournalMessages(Journal journal) {
        getNewJournalMessages(journal).stream().forEach(msg ->
                jmsTemplate.convertAndSend(ArtemisConfig.DESTINATION_NAME, msg)
        );
    }

    public void sendDailyMessages() {
        getDailyMessages().stream().forEach(msg ->
                jmsTemplate.convertAndSend(ArtemisConfig.DESTINATION_NAME, msg)
        );
    }
}

