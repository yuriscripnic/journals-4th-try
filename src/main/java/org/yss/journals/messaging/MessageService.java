package org.yss.journals.messaging;

import org.yss.journals.model.Journal;

public interface MessageService {
    void sendDailyMessages();
    void sendNewJournalMessages(Journal journal);
}
