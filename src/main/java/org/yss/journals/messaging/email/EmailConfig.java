package org.yss.journals.messaging.email;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Properties;

@ConfigurationProperties(prefix = "mail", ignoreUnknownFields = false)
public final class EmailConfig {
    private String host;
    private String port;
    private String auth;
    private String starttls_enable;
    private String ssl_trust;
    private String username;
    private String password;
    private int redeliveryDelay;
    private int maxDeliveryAttempts;


    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public String getStarttls_enable() {
        return starttls_enable;
    }

    public void setStarttls_enable(String starttls_enable) {
        this.starttls_enable = starttls_enable;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRedeliveryDelay() {
        return redeliveryDelay;
    }

    public void setRedeliveryDelay(int redeliveryDelay) {
        this.redeliveryDelay = redeliveryDelay;
    }

    public int getMaxDeliveryAttempts() {
        return maxDeliveryAttempts;
    }

    public void setMaxDeliveryAttempts(int maxDeliveryAttempts) {
        this.maxDeliveryAttempts = maxDeliveryAttempts;
    }

    public String getSsl_trust() {
        return ssl_trust;
    }

    public void setSsl_trust(String ssl_trust) {
        this.ssl_trust = ssl_trust;
    }

    public Properties getMailProperties() {
        Properties mailServerProperties = System.getProperties();
        mailServerProperties.put("mail.smtp.auth", getAuth());
        mailServerProperties.put("mail.smtp.starttls.enable", getStarttls_enable());
        mailServerProperties.put("mail.smtp.host", getHost());
        mailServerProperties.put("mail.smtp.port", getPort());
        mailServerProperties.put("mail.smtp.ssl.trust", getSsl_trust() );
        return mailServerProperties;
    }
}
