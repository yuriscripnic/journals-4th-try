package org.yss.journals.messaging.email;

import org.yss.journals.messaging.ArtemisConfig;
import org.yss.journals.messaging.MessageDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@Service
public class EmailServiceImpl implements EmailService {

    private static final Logger log = Logger.getLogger(EmailServiceImpl.class);

    private EmailConfig emailConfig;


    @Autowired
    public EmailServiceImpl(EmailConfig emailConfig){
        this.emailConfig = emailConfig;
    }

    @JmsListener(destination = ArtemisConfig.DESTINATION_NAME, containerFactory = "jmsFactory")
    public void sendEmail(MessageDTO message) throws MessagingException{
        sendEmail(message.getEmail(),message.getTitle(),message.getBody());
    }

    private Session getMailSession(){
        Properties mailServerProperties = emailConfig.getMailProperties();

        return Session.getInstance(mailServerProperties,
                new Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(emailConfig.getUsername(), emailConfig.getPassword());
                    }
                });
    }

    public synchronized void sendEmail(String email, String title, String message) throws MessagingException{
            Session mailSession = getMailSession();
            MimeMessage mailMessage;
            mailMessage = new MimeMessage(mailSession);
            mailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
            mailMessage.setSubject(title);

            mailMessage.setContent(message, "text/html");

            Transport transport = mailSession.getTransport("smtp");

            Transport.send(mailMessage);
            log.info("Email sent to: "+ email);
            transport.close();

    }


}