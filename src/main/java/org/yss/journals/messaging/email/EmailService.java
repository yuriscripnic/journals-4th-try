package org.yss.journals.messaging.email;

import javax.mail.MessagingException;

public interface EmailService  {
    void sendEmail(String email, String title, String message) throws MessagingException;
 }
