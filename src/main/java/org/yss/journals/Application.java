package org.yss.journals;

import org.yss.journals.messaging.email.EmailConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.scheduling.annotation.EnableScheduling;

@ComponentScan
@EnableConfigurationProperties({ EmailConfig.class})
@EnableAutoConfiguration
@EnableScheduling
@EnableJms
public final class Application {

    public static final String ROOT;

	static {
		ROOT = System.getProperty("upload-dir", System.getProperty("user.home") + "/upload");
	}

	public static void main(final String[] args) {
		SpringApplication app = new SpringApplicationBuilder(Application.class).build();
		app.run(args);

	}


}